package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.service.CommentService;
import chapter6.beans.User;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
		HttpSession session = request.getSession();
		User user = (User) request.getSession().getAttribute("loginUser");

		String text = request.getParameter("text");
		int userId = user.getId();
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		List<String> errorMessages = new ArrayList<String>();
		if(!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(text);
		comment.setUserId(userId);
		comment.setMessageId(messageId);

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("返信のテキストを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以内で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
