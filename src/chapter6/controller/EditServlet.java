package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("messageId");
        List<String> errorMessages = new ArrayList<String>();
        Message message = null;

        if ((!StringUtils.isBlank(id)) && (id.matches("^[0-9]+$"))) {
        	int messageId = Integer.parseInt(id);
        	message = new MessageService().select(messageId);
        }

        if(message == null) {
        	errorMessages.add("不正なパラメータが入力されました");
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("./").forward(request, response);
        	return;
        }

        request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("messageId")));
		message.setText(request.getParameter("text"));

		if (!isValid(message, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		String text = message.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以内で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
