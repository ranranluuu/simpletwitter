package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.dao.MessageDao;
import chapter6.beans.UserMessage;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;

        try {
            connection = getConnection();

            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            if (!StringUtils.isBlank(start)) {
            	start += " 00:00:00";
            } else {
            	start = "2021-12-20 00:00:00";
            }

            if (!StringUtils.isBlank(end)) {
            	end += " 23:59:59";
            } else {
            	Date defaultDate = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formatDate = format.format(defaultDate);
                end = formatDate;
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message messages = new MessageDao().select(connection, messageId);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

	    Connection connection = null;
	    try {
	        connection = getConnection();
	        new MessageDao().update(connection, message);
	        commit(connection);

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
